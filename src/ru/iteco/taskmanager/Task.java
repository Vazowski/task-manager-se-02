package ru.iteco.taskmanager;

public class Task {

    private String name;
    /*
     * Constructor
     */
    public Task(String name) {
    	this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
    	this.name = name;
    }
}
