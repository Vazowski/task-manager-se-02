package ru.iteco.taskmanager;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class Project {

    private String name;
    private List<Task> tasks;
    /*
     * Constructor
     */
    public Project(String _name) {
        this.name = _name;
        tasks = new ArrayList<>();
    }
    /*
     * Add task to project
     */
    public void createTask(String name) {
        tasks.add(new Task(name));
    }
    /*
     * Show all tasks for project
     */
    public void showTasks() {
        System.out.println("List of all tasks for project '" + this.name + "':");
        for (Task t: tasks) {
            System.out.println("\t[" + t.getName() + "]");
        }
    }
    /*
     * Update one task
     */
    public void updateTask(String name) {
        int flag = 0;
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < tasks.size(); i++) {
            if (name.equals(tasks.get(i).getName())) {
                System.out.println("Enter new name of task:");
                name = scanner.nextLine();
                tasks.get(i).setName(name);
                flag = 1;
                break;
            }
        }
        if (flag == 0)
            System.out.println("There's no task with name - " + name + "\n");
    }
    /*
     * Delete one task
     */
    public void deleteTask(String name) {
        for (int i = 0; i < tasks.size(); i++) {
            if (name.equals(tasks.get(i).getName())) {
                tasks.remove(i);
                break;
            }
        }
    }
    
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
    	this.name = name;
    }
}
