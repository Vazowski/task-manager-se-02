package ru.iteco.taskmanager;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class App
{
    private static List<Project> listOfProjects = new ArrayList<>();
    private static int currentProjectIndex;

    private static Scanner scanner;
    
    private static String projectName;

    private static final String HELP_LITERA = "help"; 
    private static final String NEW_PROJECT_OR_TASK_LITERA = "new";
    private static final String SHOW_PROJECT_OR_TASK_LITERA = "show";
    private static final String UPDATE_PROJECT_OR_TASK_LITERA = "update";
    private static final String DELETE_PROJECT_OR_TASK_LITERA = "delete";
    
    private static final String PROJECT_LITERA = "project";
    private static final String TASK_LITERA = "task";
    
    private static final String EXIT_MENU_LITERA = "up";
    private static final String TERMINATE_PROGRAMM_LITERA = "exit";
    
    public static void main( String[] args )
    {
        while(true) {
            System.out.println("Enter command (type help for more information) --- TOP");
            scanner = new Scanner(System.in);
            String userInput = scanner.nextLine();

            switch (userInput) {
                case HELP_LITERA:
                    showCommands("Main");
                    break;
                case PROJECT_LITERA:
                    while (true) {
                        if (!projectManager())
                            break;
                    }
                    break;
            }
            if (EXIT_MENU_LITERA.equals(userInput))
                break;
        }
    }
    /*
     * Manage projects and run task manager
     */
    private static boolean projectManager() {
        System.out.println("Enter command (type help for more information) --- PROJECT");
        String userInputProject = scanner.nextLine();
        switch (userInputProject) {
            case HELP_LITERA:
                showCommands("Project");
                break;
            case NEW_PROJECT_OR_TASK_LITERA:
            	createProject();
                break;
            case SHOW_PROJECT_OR_TASK_LITERA:
            	showProjects();
                break;
            case UPDATE_PROJECT_OR_TASK_LITERA:
            	updateProject();
                break;
            case DELETE_PROJECT_OR_TASK_LITERA:
            	deleteProject();
                break;
            case TASK_LITERA:
                if (!checkProjectsForEmptyAndCorrect())
                    break;
                while (true) {
                    if (!taskManager())
                        break;
                }
                break;
            case TERMINATE_PROGRAMM_LITERA:
                System.exit(0);
                break;
        }
        if (EXIT_MENU_LITERA.equals(userInputProject))
            return false;
        return true;
    }
    /*
     * Create project
     */
    private static void createProject() {
    	System.out.println("Enter name of project:");
        projectName = scanner.nextLine();
        listOfProjects.add(new Project(projectName));
    }
    /*
     * Show all projects
     */
    private static void showProjects() {
    	System.out.println("List of available projects:");
        for (Project p:listOfProjects) {
            System.out.println("\t[" + p.getName() + "]");
        }
        System.out.println();
    }
    /*
     * Update project
     */
    private static void updateProject() {
    	System.out.println("Enter the name of project that you want to update:");
        projectName = scanner.nextLine();
        int flag = 0;
        for (int i = 0; i < listOfProjects.size(); i++) {
            if (projectName.equals(listOfProjects.get(i).getName())) {
                System.out.println("Enter new name of project:");
                projectName = scanner.nextLine();
                listOfProjects.get(i).setName(projectName);
                flag = 1;
                break;
            }
        }
        if (flag == 0)
            System.out.println("There's no project with name - " + projectName + "\n");
    }
    /*
     * Delete project
     */
    private static void deleteProject() {
    	System.out.println("Enter the name of project to remove:");
        projectName = scanner.nextLine();
        for (int i = 0; i < listOfProjects.size(); i++) {
            if (projectName.equals(listOfProjects.get(i).getName())) {
                listOfProjects.remove(i);
                break;
            }
        }
    }
    /*
     * Task manager
     */
    private static boolean taskManager() {
        System.out.println("Enter command (type help for more information) --- TASK");
        String userInputTask = scanner.nextLine();
        switch (userInputTask) {
            case HELP_LITERA:
                showCommands("Task");
                break;
            case NEW_PROJECT_OR_TASK_LITERA:
                System.out.println("Enter new task:");
                String taskName = scanner.nextLine();
                listOfProjects.get(currentProjectIndex).createTask(taskName);
                break;
            case SHOW_PROJECT_OR_TASK_LITERA:
                listOfProjects.get(currentProjectIndex).showTasks();
                break;
            case UPDATE_PROJECT_OR_TASK_LITERA:
                System.out.println("Enter the task that you want to update:");
                taskName = scanner.nextLine();
                listOfProjects.get(currentProjectIndex).updateTask(taskName);
                break;
            case DELETE_PROJECT_OR_TASK_LITERA:
                System.out.println("Enter the task that you want to remove:");
                taskName = scanner.nextLine();
                listOfProjects.get(currentProjectIndex).deleteTask(taskName);
                break;
            case TERMINATE_PROGRAMM_LITERA:
                System.exit(0);
                break;
        }
        if (EXIT_MENU_LITERA.equals(userInputTask))
            return false;
        return true;
    }
    /*
     * Check for existing project
     */
    private static boolean checkProjectsForEmptyAndCorrect() {
        if (listOfProjects.size() == 0) {
            System.out.println("No project has been created yet\n");
            return false;
        }
        System.out.println("Which project you want to use:");
        for (Project p:listOfProjects) {
            System.out.println("\t[" + p.getName() + "]");
        }
        scanner = new Scanner(System.in);
        String projectName = scanner.nextLine();
        int flag = 0;
        for (int i = 0; i < listOfProjects.size(); i++) {
            if (projectName.equals(listOfProjects.get(i).getName())) {
                flag++;
                currentProjectIndex = i;
            }
        }
        if (flag == 0) {
            System.out.println("No project with that name exist");
            return false;
        }
        return true;
    }
    /*
     * Console output
     */
    private static void showCommands(String level) {
        System.out.println("List of all available commands:\n");
        switch (level) {
            case "Main":
                System.out.println("help\n\t- show help information");
                System.out.println("project\n\t- project mode");
                System.out.println("exit\n\t- exit the program\n");
                break;
            case "Project":
                System.out.println("help\n\t- show help information");
                System.out.println("new\n\t- create new project");
                System.out.println("show\n\t- show all projects");
                System.out.println("update\n\t- update project");
                System.out.println("delete\n\t- delete project");
                System.out.println("task\n\t- task mode");
                System.out.println("up\n\t- go upper\n");
                break;
            case "Task":
                System.out.println("help\n\t- show help information");
                System.out.println("new\n\t- create new task");
                System.out.println("show\n\t- show all tasks");
                System.out.println("update\n\t- update task");
                System.out.println("delete\n\t- delete task");
                System.out.println("up\n\t- go upper\n");
                break;
        }
    }
}
