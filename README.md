# Task Manager SE-02

This is another implementation of the Task Manager project.

## Software

Work OS and clean hands, actual JDK

## Stack

Java 1.7, Maven

## Deploy

maven clean install

## Launch

java -jar "task-manager-se-02.jar"

## Contacts

Denis Ermakov
